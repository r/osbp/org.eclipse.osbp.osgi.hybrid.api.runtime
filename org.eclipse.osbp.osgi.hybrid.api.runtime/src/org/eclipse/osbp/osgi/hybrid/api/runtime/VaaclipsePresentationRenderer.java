/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.osgi.hybrid.api.runtime;

import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.osbp.vaaclipse.presentation.renderers.PerspectiveStackRenderer;

import org.eclipse.osbp.osgi.hybrid.api.IPresentationRenderer;

/**
 * The Class VaaclipsePresentationRenderer.
 */
public class VaaclipsePresentationRenderer implements IPresentationRenderer {

	/**
	 * Instantiates a new vaaclipse presentation renderer.
	 */
	public VaaclipsePresentationRenderer() {
		System.err.println(VaaclipsePresentationRenderer.class.getCanonicalName());
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.osgi.hybrid.api.IPresentationRenderer#processContents(org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack, org.eclipse.e4.ui.model.application.ui.MElementContainer)
	 */
	@Override
	public void processContents(MPerspectiveStack perspectiveStack, MElementContainer<MUIElement> perspectiveStackElement) {
		try {
			PerspectiveStackRenderer perspectiveStackRenderer = (PerspectiveStackRenderer) perspectiveStack.getRenderer();
			perspectiveStackRenderer.processContents(perspectiveStackElement);
		}
		catch (Exception e) {
		}
	}
}
